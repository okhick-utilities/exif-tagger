# EXIF Tagger
This is a command line tool built with Node used to tag images with missing EXIF data. This is especially useful to add data retroactively to film scans. Please note: This will create a copy of the original with EXIF data. The original photo will remain unedited.

This is intended to (someday) be the backend for a GUI app, so there are some things that do feel a bit odd in the CLI context.

## Requirements
You should have node and NPM installed.

## Quickstart
1. Clone this repo and run `npm install`.
2. Update the **cameraProfile.json** and **userProfile.json** with your equipment and data.
3. Create your EXIF data CSV. Follow the example in the data folder. See below for more details.
4. Run 
    ```bash
    node app.js [--profiles <Place where the profile files are. Defaults to ./profiles>]
    ```
5. Answer the 5 CLI questions about Camera, Film ISO, location of data CSV, where in untagged images are, and where you want the tagged images to go.

## Profiles
### userProfile.json
The data in the userProfile will be tagged onto every image into the respective Data fields.

### cameraProfile.json
This is perhaps the oddest part of the current version. The goal here is to allow users to add their equipment to the app and to reduce redundancy in the CSV. When a user adds a camera to the cameraProfile.json, it will be used in the CLI prompts.  
For lenses, the **LensLength** field in the CSV is matched to the **LensLength** field in the json. The rest of the lens data is autmatically tagged. *Currently, each LensLength entry into the JSON must be unique. Fixing this is on my todo list.*

## CSV
There is a template in the data folder of this repository. Currently everyfield is required except for Latitude and Longitude. 
- input_name - The current name of the photo
- output_name - the name of the photo after it is tagged
- FNumber - The F-stop. Should be an number
- ExposureTime - Shutter speed. Should be formatted as a fraction. 1/60, 1/1000 etc.
- ExposureBiasValue - Number
- Flash - 1 if flash, 0 if no flash
- Date - Formatted YYYY-MM-DD
- Time - Formatted (H)H:MM AM/PM
- Latitude - Number
- Longitude - Number
- FocalLength - Number
- FocalLengthIn35mmFilm - Number
- LensLength - Number
- ImageDescription - String

## TODO
- Duplicate LensLength / Better lens handling
- filmProfile for easier tagging film scans
- Better error handling
- Build GUI app 