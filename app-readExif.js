const fs = require("fs");
const parse = require("csv-parse/lib/sync");
const ObjectsToCsv = require('objects-to-csv');

const piexif = require('piexifjs');

// =================================================================================================
// =================================================================================================
// =================================================================================================

const rawCSV = [];
let imageBin = null;

// should be one column with just a list of files with full path. No header
const readCSV = ( () => {
  let rawCSV = fs.readFileSync('./data/files.csv');
  let parsedCSV = parse(rawCSV, { columns: true });
  return parsedCSV;
}) ();

// Do actual work now

readCSV.forEach((line) => {
  readImage(line);
  readExif(line);
});

saveExifToCSV();

// =================================================================================================
// =================================================================================================
// =================================================================================================

function readImage(line) {
  let raw = fs.readFileSync(line.path);
  imageBin = raw.toString('binary');
}

function readExif(line) {
  let exifObject = {};
  exifObject.fileName = getFileNameFromPath(line.path)
  let allExistingExif = piexif.load(imageBin);

  //loop through all data types and extract all data into a human readable/workable format
  for (let ifd in allExistingExif) {

    // eh just do exif for now...
    if (ifd == "Exif") {
      for (let tag in allExistingExif[ifd]) {
        let key = piexif.TAGS[ifd][tag]["name"];

        // MakerNote is a binary or something. Skip it.
        if (key != 'MakerNote') {
          exifObject[key] = allExistingExif[ifd][tag];
        }
      }
    }
  }
  rawCSV.push(exifObject);
  console.log(`${exifObject.fileName} has been read.`)
}

function getFileNameFromPath(path) {
  const splitPath = path.split("/");
  return splitPath[splitPath.length - 1];
}

async function saveExifToCSV() {
  const csv = new ObjectsToCsv(rawCSV);
  await csv.toDisk('./exif_data.csv');
  console.log("Done!")
}