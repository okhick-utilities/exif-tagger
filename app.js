const fs = require("fs");
const parse = require("csv-parse/lib/sync");
const program = require('commander');

const { Image } = require('./src/exif-image');

const { CLI } = require('./src/cli');

program
  .option('--profile <directory>', 'Place where the profile files are', './profiles')
  .option('--readOnly', 'Only reads exif data');

program.parse(process.argv);

if (program.readOnly) {
  readExif();
  return;
}

//load up the user
const user = ( () => {
  let rawUser = fs.readFileSync(`${program.profile}/userProfile.json`);
  let parsedUser = JSON.parse(rawUser);
  return parsedUser;
}) ();

//load up the camera profile
const cameraInfo = ( () => {
  let rawCamera = fs.readFileSync(`${program.profile}/cameraProfile.json`);
  let parsedCamera = JSON.parse(rawCamera);
  return parsedCamera;
}) ();

const CLIInterface = new CLI(cameraInfo);

CLIInterface.getUserInput().then((userInput) => {
  // load up the photo info
  const csv = ( () => {
    let rawCSV = fs.readFileSync(`${userInput.csvLoc.trim()}`);
    let parsedCSV = parse(rawCSV, { columns: true });
    return parsedCSV;
  }) ();

  // process the photos
  let counter = 0;
  csv.forEach( (csvRowData) => {
    processImage(csvRowData, userInput);
    counter++;
    if (counter === csv.length) {
      console.log('All images have been tagged.');
    }
  });
});

// =====================================================================|
// =====================================================================|
// =====================================================================|

function processImage(csvRowData, userInput) {
  try {

    const zerothArgs = {
      name: userInput.camera,
      ImageDescription: csvRowData.ImageDescription,
      Artist: user.Artist,
      Copyright: user.Copyright
    }

    const exifArgs = {
      DateTimeOriginal: { date:csvRowData.Date, time:csvRowData.Time },
      ExposureTime: csvRowData.ExposureTime,
      FNumber: csvRowData.FNumber,
      Flash: csvRowData.Flash,
      ISOSpeedRatings: userInput.iso,
      FocalLength: csvRowData.FocalLength,
      FocalLengthIn35mmFilm: csvRowData.FocalLengthIn35mmFilm,
      ExposureBiasValue: csvRowData.ExposureBiasValue,
      LensLength: csvRowData.LensLength
    }

    const gpsArgs = {
      Latitude: csvRowData.Latitude,
      Longitude: csvRowData.Longitude
    }

    const IO = {
      input: `${userInput.input.trim()}/${csvRowData.input_name}`,
      output: `${userInput.output.trim()}/${csvRowData.output_name}`
    }

    const image = new Image(IO, zerothArgs, exifArgs, gpsArgs, cameraInfo);
      image.readImage();
      image.readExif();
      image.prepareNewZeroth();
      image.prepareNewExif();
      try {
        image.prepareNewGPS();
      } catch (e) {
        console.log("No GPS data? Looks like it...");
      }
      image.swapExifIds();
      image.generateImageWithExif();
      image.saveImageWithExif();

    console.log(`${IO.output} has been tagged!`);

  } catch (e) {
    console.log(e);
  }
}

function readExif() {
  const image = {input: './20190901_5.JPG'}
  const testImage = new Image(image, {}, {});
  testImage.readImage();
  testImage.readExif();
  testImage.printExif();
}
