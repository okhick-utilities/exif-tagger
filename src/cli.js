const inquirer = require('inquirer');
const fs = require('fs')

// Making this a class was probably overkill. Oh well...
class CLI {

  constructor(cameraInfo) {
    this.cameraInfo = cameraInfo;
  }

  async getUserInput() {
    const responses = await inquirer
      .prompt([
        {
          type: 'list',
          message: 'Select Camera:',
          name: 'camera',
          choices: [
            ...this.cameraInfo.camera
          ],
          validate: function(path) {
            if (path.length < 1) {
              return 'You must choose at least one camera.';
            }
            return true;
          },
        },
        {
          type: 'number',
          message: 'What was the film ISO?',
          name: "iso",
          validate: function(iso) {
            if (!Number.isInteger(iso)) {
              return "Please enter a valid integer"
            }
            return true
          }
        },
        {
          type: 'input',
          message: 'Where is the CSV?',
          name: 'csvLoc',
          validate: function(path) {
            if (!fs.existsSync(path.trim())) {
              return 'CSV does not seem to exist!'
            }
            return true
          } 
        },
        {
          type: 'input',
          message: 'What folder are the images in?',
          name: 'input',
          validate: function (path) {
            if (!fs.existsSync(path.trim())) {
              return 'Directory does not seem to exist!'
            }
            return true
          },
        },
        {
          type: 'input',
          mesage: 'Where do you want the new images?',
          name: 'output',
          validate: function (path) {
            if (!fs.existsSync(path.trim())) {
              return 'Directory does not seem to exist!'
            }
            return true
          },
        }
      ]);
    
    return responses;
  }

}

module.exports = { CLI }


